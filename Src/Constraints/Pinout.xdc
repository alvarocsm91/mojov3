############################################################################
# * Header
############################################################################
#!  \project        Mojo V3
#!  \file           Pinout.ucf
#!  \author         Álvaro Cortés Sánchez-Migallón
#!  \email          alvarocsm.91@gmail.com
#!  \version        01 / A
#!  \date           11/05/2020
#|##########################################################################
#|  Description:
#!
#!  \class         Constraint file
#!
#!  \brief         This module represent the FPGA Mojo V3 interface. This
#!                 file to define PFGA pinout constranit. It have been
#!                 created following the schematic pdf. Represent hardware
#!                 interface between Spartan 6 FPGA and Mojo V3 PCB.
#!
#!  \section links
#!
#!  \section extra More information in Hardware schemaic specification
#!                 document and board documentation.
#!
#'##########################################################################
#
# * Bank 0
# ** Supply pin
#NET VCCO_0                    LOC = "P122" | IOSTANDARD = LVCMOS25; # "VCCO_0"
#NET VCCO_0                    LOC = "P125" | IOSTANDARD = LVCMOS25; # "VCCO_0"
#NET VCCO_0                    LOC = "P135" | IOSTANDARD = LVCMOS25; # "VCCO_0"
# ** Functional pin
NET IO_L1P_HSWAPEN_0           LOC = "P144" | IOSTANDARD = LVCMOS25; # "L1_P_0"
NET IO_L1N_VREF_0              LOC = "P143" | IOSTANDARD = LVCMOS25; # "L1_N_0"
NET IO_L2P_0                   LOC = "P142" | IOSTANDARD = LVCMOS25; # "L2_P_0"
NET IO_L2N_0                   LOC = "P141" | IOSTANDARD = LVCMOS25; # "L2_N_0"
NET IO_L3P_0                   LOC = "P140" | IOSTANDARD = LVCMOS25; # "L3_P_0"
NET IO_L3N_0                   LOC = "P139" | IOSTANDARD = LVCMOS25; # "L3_N_0"
NET IO_L4P_0                   LOC = "P138" | IOSTANDARD = LVCMOS25; # "L4_P_0"
NET IO_L4N_0                   LOC = "P137" | IOSTANDARD = LVCMOS25; # "L4_N_0"
NET IO_L34P_GCLK19_0           LOC = "P134" | IOSTANDARD = LVCMOS25; # "L34_P_0"
NET IO_L34N_GCLK18_0           LOC = "P133" | IOSTANDARD = LVCMOS25; # "L34_N_0"
NET IO_L35P_GCLK17_0           LOC = "P132" | IOSTANDARD = LVCMOS25; # "L35_P_0"
NET IO_L35N_GCLK16_0           LOC = "P131" | IOSTANDARD = LVCMOS25; # "L35_N_0"
NET IO_L36P_GCLK15_0           LOC = "P127" | IOSTANDARD = LVCMOS25; # "L36_P_0"
NET IO_L36N_GCLK14_0           LOC = "P126" | IOSTANDARD = LVCMOS25; # "L36_N_0"
NET IO_L37P_GCLK13_0           LOC = "P124" | IOSTANDARD = LVCMOS25; # "L37_P_0"
NET IO_L37N_GCLK12_0           LOC = "P123" | IOSTANDARD = LVCMOS25; # "L37_N_0"
NET IO_L62P_0                  LOC = "P121" | IOSTANDARD = LVCMOS25; # "L62_P_0"
NET IO_L62N_VREF_0             LOC = "P120" | IOSTANDARD = LVCMOS25; # "L62_N_0"
NET IO_L63P_SCP7_0             LOC = "P119" | IOSTANDARD = LVCMOS25; # "L63_P_0"
NET IO_L63N_SCP6_0             LOC = "P118" | IOSTANDARD = LVCMOS25; # "L63_N_0"
NET IO_L64P_SCP5_0             LOC = "P117" | IOSTANDARD = LVCMOS25; # "L64_P_0"
NET IO_L64N_SCP4_0             LOC = "P116" | IOSTANDARD = LVCMOS25; # "L64_N_0"
NET IO_L65P_SCP3_0             LOC = "P115" | IOSTANDARD = LVCMOS25; # "L65_P_0"
NET IO_L65N_SCP2_0             LOC = "P114" | IOSTANDARD = LVCMOS25; # "L65_N_0"
NET IO_L66P_SCP1_0             LOC = "P112" | IOSTANDARD = LVCMOS25; # "L66_P_0"
NET IO_L66N_SCP0_0             LOC = "P111" | IOSTANDARD = LVCMOS25; # "L66_N_0"

# * Bank 1
# ** Supply pin
#NET VCCO_1                    LOC = "P76"  | IOSTANDARD = LVCMOS25; # "VCCO_1"
#NET VCCO_1                    LOC = "P86"  | IOSTANDARD = LVCMOS25; # "VCCO_1"
#NET VCCO_1                    LOC = "P103" | IOSTANDARD = LVCMOS25; # "VCCO_1"
# ** Functional pin
NET IO_L1P_1                   LOC = "P105" | IOSTANDARD = LVCMOS25; # "L1P_1"
NET IO_L1N_VREF_1              LOC = "P104" | IOSTANDARD = LVCMOS25; # "L1N_1"
NET IO_L32P_1                  LOC = "P102" | IOSTANDARD = LVCMOS25; # "L32P_1"
NET IO_L32N_1                  LOC = "P101" | IOSTANDARD = LVCMOS25; # "L32N_1"
NET IO_L33P_1                  LOC = "P100" | IOSTANDARD = LVCMOS25; # "L33P_1"
NET IO_L33N_1                  LOC = "P99"  | IOSTANDARD = LVCMOS25; # "L33N_1"
NET IO_L34P_1                  LOC = "P98"  | IOSTANDARD = LVCMOS25; # "L34P_1"
NET IO_L34N_1                  LOC = "P97"  | IOSTANDARD = LVCMOS25; # "L34N_1"
NET IO_L40P_GCLK11_1           LOC = "P95"  | IOSTANDARD = LVCMOS25; # "L40P_1"
NET IO_L40N_GCLK10_1           LOC = "P94"  | IOSTANDARD = LVCMOS25; # "L40N_1"
NET IO_L41P_GCLK9_IRDY1_1      LOC = "P93"  | IOSTANDARD = LVCMOS25; # "L41P_1"
NET IO_L41N_GCLK8_1            LOC = "P92"  | IOSTANDARD = LVCMOS25; # "L41N_1"
NET IO_L42P_GCLK7_1            LOC = "P88"  | IOSTANDARD = LVCMOS25; # "L42P_1"
NET IO_L42N_GCLK6_TRDY1_1      LOC = "P87"  | IOSTANDARD = LVCMOS25; # "L42N_1"
NET IO_L43P_GCLK5_1            LOC = "P85"  | IOSTANDARD = LVCMOS25; # "L43P_1"
NET IO_L43N_GCLK4_1            LOC = "P84"  | IOSTANDARD = LVCMOS25; # "L43N_1"
NET IO_L45P_1                  LOC = "P83"  | IOSTANDARD = LVCMOS25; # "L45P_1"
NET IO_L45N_1                  LOC = "P82"  | IOSTANDARD = LVCMOS25; # "L45N_1"
NET IO_L46P_1                  LOC = "P81"  | IOSTANDARD = LVCMOS25; # "L46P_1"
NET IO_L46N_1                  LOC = "P80"  | IOSTANDARD = LVCMOS25; # "L46N_1"
NET IO_L47P_1                  LOC = "P79"  | IOSTANDARD = LVCMOS25; # "L47P_1"
NET IO_L47N_1                  LOC = "P78"  | IOSTANDARD = LVCMOS25; # "L47N_1"
NET IO_L74P_AWAKE_1            LOC = "P75"  | IOSTANDARD = LVCMOS25; # "L74P_1"
NET IO_L74N_DOUT_BUSY_1        LOC = "P74"  | IOSTANDARD = LVCMOS25; # "L74N_1"

# * Bank 2
# ** Supply pin
#NET VCCO_2                    LOC = "P42" | IOSTANDARD = LVCMOS25; # "VCCO_2"
#NET VCCO_2                    LOC = "P63" | IOSTANDARD = LVCMOS25; # "VCCO_2"
# ** Functional pin
NET CMPCS_B_2                  LOC = "P72" | IOSTANDARD = LVCMOS25; # "VCCO_2"
NET DONE_2                     LOC = "P71" | IOSTANDARD = LVCMOS25; # "DONE"
NET PROGRAM_B_2                LOC = "P37" | IOSTANDARD = LVCMOS25; # "PROGRAM_B"
NET IO_L1P_CCLK_2              LOC = "P70" | IOSTANDARD = LVCMOS25; # "CCLK"
NET IO_L1N_M0_CMPMISO_2        LOC = "P69" | IOSTANDARD = LVCMOS25; # ""
NET IO_L2P_CMPCLK_2            LOC = "P67" | IOSTANDARD = LVCMOS25; # "L2P_2"
NET IO_L2N_CMPMOSI_2           LOC = "P66" | IOSTANDARD = LVCMOS25; # "L2N_2"
NET IO_L3P_D0_DIN_MISO_MISO1_2 LOC = "P65" | IOSTANDARD = LVCMOS25; # "D0"
NET IO_L3N_MOSI_CSI_B_MISO0_2  LOC = "P64" | IOSTANDARD = LVCMOS25; # "GND"
NET IO_L12P_D1_MISO2_2         LOC = "P62" | IOSTANDARD = LVCMOS25; # "D1"
NET IO_L12N_D2_MISO3_2         LOC = "P61" | IOSTANDARD = LVCMOS25; # "D2"
NET IO_L13P_M1_2               LOC = "P60" | IOSTANDARD = LVCMOS25; # ""
NET IO_L13N_D10_2              LOC = "P59" | IOSTANDARD = LVCMOS25; # "AVR_RX"
NET IO_L14P_D11_2              LOC = "P58" | IOSTANDARD = LVCMOS25; # "L14P_2"
NET IO_L14N_D12_2              LOC = "P57" | IOSTANDARD = LVCMOS25; # "L14N_2"
NET IO_L30P_GCLK1_D13_2        LOC = "P56" | IOSTANDARD = LVCMOS25; # "CLK"
NET IO_L30N_GCLK0_USERCCLK_2   LOC = "P55" | IOSTANDARD = LVCMOS25; # "AVR_TX"
NET IO_L31P_GCLK31_D14_2       LOC = "P51" | IOSTANDARD = LVCMOS25; # "L31P_2"
NET IO_L31N_GCLK30_D15_2       LOC = "P50" | IOSTANDARD = LVCMOS25; # "L31N_2"
NET IO_L48P_D7_2               LOC = "P48" | IOSTANDARD = LVCMOS25; # "SS"
NET IO_L48N_RDWR_B_VREF_2      LOC = "P47" | IOSTANDARD = LVCMOS25; # ""
NET IO_L49P_D3_2               LOC = "P46" | IOSTANDARD = LVCMOS25; # "D3"
NET IO_L49N_D4_2               LOC = "P45" | IOSTANDARD = LVCMOS25; # "MISO"
NET IO_L62P_D5_2               LOC = "P44" | IOSTANDARD = LVCMOS25; # "MOSI"
NET IO_L62N_D6_2               LOC = "P43" | IOSTANDARD = LVCMOS25; # "SCK"
NET IO_L64P_D8_2               LOC = "P41" | IOSTANDARD = LVCMOS25; # "L64P_2"
NET IO_L64N_D9_2               LOC = "P40" | IOSTANDARD = LVCMOS25; # "L64N_2"
NET IO_L65P_INIT_B_2           LOC = "P39" | IOSTANDARD = LVCMOS25; # "INIT_B"
NET IO_L65N_CSO_B_2            LOC = "P38" | IOSTANDARD = LVCMOS25; # "L65N_2"

# * Bank 3
# ** Supply pin
#NET VCCO_3                    LOC = "P4"  | IOSTANDARD = LVCMOS25; # "VCCO_3"
#NET VCCO_3                    LOC = "P18" | IOSTANDARD = LVCMOS25; # "VCCO_3"
#NET VCCO_3                    LOC = "P31" | IOSTANDARD = LVCMOS25; # "VCCO_3"
# ** Functional pin
NET IO_L1P_3                   LOC = "P35" | IOSTANDARD = LVCMOS25; # "L1P_3"
NET IO_L1N_VREF_3              LOC = "P34" | IOSTANDARD = LVCMOS25; # "L1N_3"
NET IO_L2P_3                   LOC = "P33" | IOSTANDARD = LVCMOS25; # "L2P_3"
NET IO_L2N_3                   LOC = "P32" | IOSTANDARD = LVCMOS25; # "L2N_3"
NET IO_L36P_3                  LOC = "P30" | IOSTANDARD = LVCMOS25; # "L36P_3"
NET IO_L36N_3                  LOC = "P29" | IOSTANDARD = LVCMOS25; # "L36N_3"
NET IO_L37P_3                  LOC = "P27" | IOSTANDARD = LVCMOS25; # "L37P_3"
NET IO_L37N_3                  LOC = "P26" | IOSTANDARD = LVCMOS25; # "L37N_3"
NET IO_L41P_GCLK27_3           LOC = "P24" | IOSTANDARD = LVCMOS25; # "L41P_3"
NET IO_L41N_GCLK26_3           LOC = "P23" | IOSTANDARD = LVCMOS25; # "L41N_3"
NET IO_L42P_GCLK25_TRDY2_3     LOC = "P22" | IOSTANDARD = LVCMOS25; # "L42P_3"
NET IO_L42N_GCLK24_3           LOC = "P21" | IOSTANDARD = LVCMOS25; # "L42N_3"
NET IO_L43P_GCLK23_3           LOC = "P17" | IOSTANDARD = LVCMOS25; # "L43P_3"
NET IO_L43N_GCLK22_IRDY2_3     LOC = "P16" | IOSTANDARD = LVCMOS25; # "L43N_3"
NET IO_L44P_GCLK21_3           LOC = "P15" | IOSTANDARD = LVCMOS25; # "L44P_3"
NET IO_L44N_GCLK20_3           LOC = "P14" | IOSTANDARD = LVCMOS25; # "L44N_3"
NET IO_L49P_3                  LOC = "P12" | IOSTANDARD = LVCMOS25; # "L49P_3"
NET IO_L49N_3                  LOC = "P11" | IOSTANDARD = LVCMOS25; # "L49N_3"
NET IO_L50P_3                  LOC = "P10" | IOSTANDARD = LVCMOS25; # "L50P_3"
NET IO_L50N_3                  LOC = "P9"  | IOSTANDARD = LVCMOS25; # "L50N_3"
NET IO_L51P_3                  LOC = "P8"  | IOSTANDARD = LVCMOS25; # "L51P_3"
NET IO_L51N_3                  LOC = "P7"  | IOSTANDARD = LVCMOS25; # "L51N_3"
NET IO_L52P_3                  LOC = "P6"  | IOSTANDARD = LVCMOS25; # "L52P_3"
NET IO_L52N_3                  LOC = "P5"  | IOSTANDARD = LVCMOS25; # "L52N_3"
NET IO_L83P_3                  LOC = "P2"  | IOSTANDARD = LVCMOS25; # "L83P_3"
NET IO_L83N_VREF_3             LOC = "P1"  | IOSTANDARD = LVCMOS25; # "L83N_3"

# * Bank VCCAUX
#NET VCCAUX                    LOC = "P20"  ; # "3V3"
#NET VCCAUX                    LOC = "P36"  ; # "3V3"
#NET VCCAUX                    LOC = "P53"  ; # "3V3"
#NET VCCAUX                    LOC = "P90"  ; # "3V3"
#NET VCCAUX                    LOC = "P129" ; # "3V3"

# * Bank VCCINT
#NET VCCINT                    LOC = "P19"  ; # "VCCINT"
#NET VCCINT                    LOC = "P18"  ; # "VCCINT"
#NET VCCINT                    LOC = "P52"  ; # "VCCINT"
#NET VCCINT                    LOC = "P89"  ; # "VCCINT"
#NET VCCINT                    LOC = "P128" ; # "VCCINT"

# * Bank GND
#NET GND                       LOC "P3"; # GND
#NET GND                       LOC "P13"; # GND
#NET GND                       LOC "P25" ; # GND
#NET GND                       LOC "P49" ; # GND
#NET GND                       LOC "P54" ; # GND
#NET GND                       LOC "P68" ; # GND
#NET GND                       LOC "P77" ; # GND
#NET GND                       LOC "P91" ; # GND
#NET GND                       LOC "P96" ; # GND
#NET GND                       LOC "P108" ; # GND
#NET GND                       LOC "P113" ; # GND
#NET GND                       LOC "P130" ; # GND
#NET GND                       LOC "P136" ; # GND

# * Bank NA
#NET SUSPEND                   LOC = "P73"   ; # SUSPEND
#NET TCK                       LOC     = "P109"  ; # TCK
#NET TDI                       LOC     = "P110"  ; # TDI
#NET TDO                       LOC     = "P106"  ; # TDO
#NET TMS                       LOC     = "P107"  ; # TMS
