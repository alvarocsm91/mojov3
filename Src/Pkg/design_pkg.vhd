-------------------------------------------------------------------------------
-- * Header
-------------------------------------------------------------------------------
--!  \project        Mojo V3
--!  \file           design_pkg.vhd
--!  \author         Alvaro Cortes Sanchez-Migallon
--!  \email          alvarocsm.91@gmail.com
--!  \version        A/0
--!  \date           13/05/2020
--|----------------------------------------------------------------------------
--|  Description:
--!
--!  \class         VHDL setup file
--!
--!  \brief         In this file are defined the constants used in this project
--!
--!  \section links links
--!
--!  \section extra More information in Firmware specification document and
--!                 board documentation.
--!
--'---------------------------------------------------------------------------'

-------------------------------------------------------------------------------
-- * Package Definition:
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * Libraries used:                                                         --
-------------------------------------------------------------------------------
-- ** Standar VHDL Packages
library IEEE                                                                   ;
use IEEE.std_logic_1164.all                                                    ;
--use IEEE.std_logic_textio.all                                                ;
--use IEEE.std_logic_arith.all                                                 ;
--use IEEE.numeric_bit.all                                                     ;
use IEEE.numeric_std.all                                                       ;
--use IEEE.std_logic_signed.all                                                ;
--use IEEE.std_logic_unsigned.all                                              ;
--use IEEE.math_real.all                                                       ;
--use IEEE.math_complex.all                                                    ;

--library STD                                                                  ;
--use STD.textio                                                               ;

-------------------------------------------------------------------------------
-- * Package
-------------------------------------------------------------------------------
package design_pkg is

  -- ** Global constant
  constant C_NIBBLE_B : integer := 4;   -- Bits in a nibble or half word
  constant C_BYTE_B   : integer := 8;   -- Bits in a byte
  constant C_WORD_B   : integer := 16;  -- Bits in a word
  constant C_DWORD_B  : integer := 32;  -- Bits in a double word

  -- ** HAL constant
  constant C_SEC_BIN : unsigned(C_DWORD_B-1 downto 0) := x"02FAF080"           ;
  constant C_NUM_LED : integer                        := 8;  -- Number of leds in PCB

  -- *** Clk Manager
  constant C_CLK_DIV    : integer := 4                                         ;
  constant C_CLK_DIV_DV : integer := 4                                         ;
  constant C_CLK_MUL    : integer := 4                                         ;
  constant C_PERIOD_IN : real := 20.0;  -- ns

  -- ** User Constants
  constant C_FW_VER : std_logic_vector(C_BYTE_B-1 downto 0) := x"00"           ;

end package design_pkg                                                         ;
