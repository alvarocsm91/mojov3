-------------------------------------------------------------------------------
-- * Header
-------------------------------------------------------------------------------
--!  \project        Mojo V3
--!  \file           HAL.vhd
--!  \author         Alvaro Cortes Sanchez-Migallon
--!  \email          alvarocsm.91@gmail.com
--!  \version        A/0
--!  \date           25/04/2020
--|----------------------------------------------------------------------------
--|  Description:
--!
--!  \class         VHDL source file
--!
--!  \brief         This file is used to instance specific hardware of Mojo V3
--!                 SPARTAN 6 FGPA as clock resources or IO resources
--!
--!  \section links links
--!
--!  \section extra More information in Firmware specification document and
--!                 board documentation.
--!
--!  \section buses buses
--!
--'---------------------------------------------------------------------------'

-------------------------------------------------------------------------------
-- * Libraries used:
-------------------------------------------------------------------------------
-- ** Standar VHDL Packages
library IEEE                                                                   ;
use IEEE.std_logic_1164.all                                                    ;
--use IEEE.std_logic_textio.all                                                ;
--use IEEE.std_logic_arith.all                                                 ;
--use IEEE.numeric_bit.all                                                     ;
use IEEE.numeric_std.all                                                       ;
--use IEEE.std_logic_signed.all                                                ;
--use IEEE.std_logic_unsigned.all                                              ;
--use IEEE.math_real.all                                                       ;
--use IEEE.math_complex.all                                                    ;

--library STD                                                                  ;
--use STD.textio                                                               ;

-- ** Primitive components
--library unisim                                                               ;
--use unisim.vcomponents.all                                                   ;

-------------------------------------------------------------------------------
-- * Packages used
-------------------------------------------------------------------------------
use work.design_pkg.all                                                        ;

-------------------------------------------------------------------------------
-- * Entity definition
-------------------------------------------------------------------------------
entity HAL is
  port (
    --!\name Bank 0
    --!{
    CLK     : in  std_logic                                                    ;
    RESET_N : in  std_logic                                                    ;
    LED     : out std_logic_vector(C_NUM_LED-1 downto 0)
   --!}
    )                                                                          ;
end entity HAL                                                                 ;

-------------------------------------------------------------------------------
-- * Architecture body:
-------------------------------------------------------------------------------
architecture RTL of HAL is

  -- ** Constant definition
  -- none here, (see ./../../IP/Pkg/design_pkg.vhd)

  -- ** Signal definition
  --!\name Internal signals
  --!{
  signal Reset_n_loc : std_logic                                               ;
  signal Clk50MHz    : std_logic                                               ;
  signal Clk100MHz   : std_logic                                               ;
  --!}

-------------------------------------------------------------------------------
-- * Arch Begin
-------------------------------------------------------------------------------
begin

  -- ** Signal association

  -- *** Internal to outputs

  -- *** Inputs to internal

  -- ** Component Instantation
  -- *** UserTop
  uUserTop : entity work.UserTop
    port map (
      Clk50MHz  => Clk50MHz                                                    ,
      Clk100MHz => Clk100MHz                                                   ,
      Reset_n   => Reset_n_loc                                                 ,
      Led       => Led)                                                        ;

  -- *** Clk Manager
  uClkManager : entity work.ClkManager
    port map (
      CLK         => CLK                                                       ,
      RESET_N     => RESET_N                                                   ,
      Clk50MHz    => Clk50MHz                                                  ,
      Clk100MHz   => Clk100MHz                                                 ,
      Reset_n_loc => Reset_n_loc)                                              ;

end architecture RTL                                                           ;
-- * End of File
