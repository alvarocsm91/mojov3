-------------------------------------------------------------------------------
-- * Header
-------------------------------------------------------------------------------
--!  \project        Mojo V3
--!  \file           ClkManager.vhd
--!  \author         Alvaro Cortes Sanchez-Migallon
--!  \email          alvarocsm.91@gmail.com
--!  \version        A/0
--!  \date           25/04/2020
--|----------------------------------------------------------------------------
--|  Description:
--!
--!  \class         VHDL source file
--!
--!  \brief         This file is used to instance specific hardware of Mojo V3
--!                 SPARTAN 6 FGPA as clock resources or IO resources
--!
--!
--!                 Equation DCM_CLKGEN:
--!                 F_clkfx = (F_clkin * CLKFX_MULTIPLY) / CLKFXDV_DIVIDE
--!
--!
--!  \section links links
--!
--!  \section extra More information in Firmware specification document and
--!                 board documentation.
--!
--!  \section buses buses
--!
--'---------------------------------------------------------------------------'

-------------------------------------------------------------------------------
-- * Libraries used:
-------------------------------------------------------------------------------
-- ** Standar VHDL Packages
library IEEE                                                                   ;
use IEEE.std_logic_1164.all                                                    ;
--use IEEE.std_logic_textio.all                                                ;
--use IEEE.std_logic_arith.all                                                 ;
--use IEEE.numeric_bit.all                                                     ;
use IEEE.numeric_std.all                                                       ;
--use IEEE.std_logic_signed.all                                                ;
--use IEEE.std_logic_unsigned.all                                              ;
--use IEEE.math_real.all                                                       ;
--use IEEE.math_complex.all                                                    ;

--library STD                                                                  ;
--use STD.textio                                                               ;

-- ** Primitive components
--library unisim                                                                 ;
--use unisim.vcomponents.all                                                     ;

-------------------------------------------------------------------------------
-- * Packages used
-------------------------------------------------------------------------------
use work.design_pkg.all                                                        ;

-------------------------------------------------------------------------------
-- * Entity definition
-------------------------------------------------------------------------------
entity ClkManager is
  port (
    --!\name Input signals
    --!{
    CLK         : in  std_logic                                                ;
    RESET_N     : in  std_logic                                                ;
    --!}
    --!\name Output signals
    --!{
    Clk50MHz    : out std_logic                                                ;
    Clk100MHz   : out std_logic                                                ;
    Reset_n_loc : out std_logic
   --!}
    )                                                                          ;
end entity ClkManager                                                          ;

-------------------------------------------------------------------------------
-- * Architecture body:
-------------------------------------------------------------------------------
architecture RTL of ClkManager is

  -- ** Constant definition
  -- none here, (see ./../../IP/Pkg/design_pkg.vhd)

  -- ** Signal definition
  --!\name Internal signals
  --!{
  signal ClkIn        : std_logic                                              ;
  signal Clk50MHzDcm  : std_logic                                              ;
  signal Clk100MHzDcm : std_logic                                              ;
  --!}

-------------------------------------------------------------------------------
-- * Arch Begin
-------------------------------------------------------------------------------
begin

  -- ** Signal association

  -- *** Internal to outputs

  -- *** Inputs to internal

  -- ** Component Instantation
  -- *** IBUFG
  -- IBUFG: Single-ended global clock input buffer
  -- Spartan-6
  -- Xilinx HDL Libraries Guide, version 14.7
  uIBUFG : IBUFG
    generic map (
      IBUF_LOW_PWR => TRUE,  -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O stand
      IOSTANDARD   => "DEFAULT")
    port map (
      O => ClkIn                                                               ,
      I => CLK)                                                                ;

  -- *** DCM
--  -- DCM_CLKGEN: Frequency Aligned Digital Clock Manager
--  -- Spartan-6
--  -- Xilinx HDL Libraries Guide, version 14.7
--  uDCM_CLKGEN : DCM_CLKGEN
--    generic map (
--      CLKFXDV_DIVIDE  => C_CLK_DIV_DV,  -- CLKFXDV divide value (2, 4, 8, 16, 32)
--      CLKFX_DIVIDE    => C_CLK_DIV,     -- Divide value - D - (1-256)
--      CLKFX_MD_MAX    => 0.0,  -- Specify maximum M/D ratio for timing anlysis
--      CLKFX_MULTIPLY  => C_CLK_MUL,     -- Multiply value - M - (2-256)
--      CLKIN_PERIOD    => C_PERIOD_IN,   -- Input clock period specified in nS
--      SPREAD_SPECTRUM => "NONE",  -- Spread Spectrum mode "NONE", "CENTER_LOW_SPREAD", "CENTER_HIGH_SPREAD", -- "VIDEO_LINK_M0", "VIDEO_LINK_M1" or "VIDEO_LINK_M2"
--      STARTUP_WAIT    => FALSE)  -- Delay config DONE until DCM_CLKGEN LOCKED (TRUE/FALSE)
--    port map (
--      --!\name Input signals
--      --!{
--      CLKIN     => ClkIn,               -- 1-bit input: Input clock
--      FREEZEDCM => '0',  -- 1-bit input: Prevents frequency adjustments to input clock
--      PROGCLK   => '0',  -- 1-bit input: Clock input for M/D reconfiguration
--      PROGDATA  => '0',  -- 1-bit input: Serial data input for M/D reconfiguration
--      PROGEN    => Loopback,  -- 1-bit input: Active high program enable
--      RST       => not RESET_N,         -- 1-bit input: Reset input pin
--      --!}
--      --!\name Output signals
--      --!{
--      CLKFX     => Clk100MHzDcm,        -- 1-bit output: Generated clock output
--      CLKFX180  => open,  -- 1-bit output: Generated clock output 180 degree out of phase from CLKFX.
--      CLKFXDV   => Clk50MHzDcm,         -- 1-bit output: Divided clock output
--      LOCKED    => Reset_n,             -- 1-bit output: Locked output
--      PROGDONE  => Loopback,  -- 1-bit output: Active high output to indicate the successful re-programming
--      STATUS    => open);               -- 2-bit output: DCM_CLKGEN status

  -- *** DCM SP
  -- DCM_SP: Digital Clock Manager
  -- Spartan-6
  -- Xilinx HDL Libraries Guide, version 14.7
  uDCM_SP : DCM_SP
    generic map (
      CLKDV_DIVIDE          => C_CLK_DIV_DV,  -- CLKDV divide value -- (1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,9,10,11,12,13,14,15,16).
      CLKFX_DIVIDE          => C_CLK_DIV_DV,  -- Divide value on CLKFX outputs - D - (1-32)
      CLKFX_MULTIPLY        => C_CLK_MUL,  -- Multiply value on CLKFX outputs - M - (2-32)
      CLKIN_DIVIDE_BY_2     => FALSE,   -- CLKIN divide by two (TRUE/FALSE)
      CLKIN_PERIOD          => C_PERIOD_IN,  -- Input clock period specified in nS
      CLKOUT_PHASE_SHIFT    => "NONE",  -- Output phase shift (NONE, FIXED, VARIABLE)
      CLK_FEEDBACK          => "1X",    -- Feedback source (NONE, 1X, 2X)
      DESKEW_ADJUST         => "SYSTEM_SYNCHRONOUS",  -- SYSTEM_SYNCHRNOUS or SOURCE_SYNCHRONOUS
      DFS_FREQUENCY_MODE    => "LOW",   -- Unsupported - Do not change value
      DLL_FREQUENCY_MODE    => "LOW",  -- Unsupported - Do not change value DSS_MODE => "NONE", -- Unsupported - Do not change value
      DUTY_CYCLE_CORRECTION => TRUE,    -- Unsupported - Do not change value
      FACTORY_JF            => X"c080",    -- Unsupported - Do not change value
      PHASE_SHIFT           => 0,  -- Amount of fixed phase shift (-255 to 255)
      STARTUP_WAIT          => FALSE)  -- Delay config DONE until DCM_SP LOCKED (TRUE/FALSE)
    port map (
      --!\name Input signals
      --!{
      CLKFB    => Clk50MHzDcm,          -- 1-bit input: Clock feedback input
      CLKIN    => ClkIn,                -- 1-bit input: Clock input
      --DSSEN    => DSSEN, -- 1-bit input: Unsupported, specify to GND.
      PSCLK    => '0',                  -- 1-bit input: Phase shift clock input
      PSEN     => '0',                  -- 1-bit input: Phase shift enable
      PSINCDEC => '0',   -- 1-bit input: Phase shift increment/decrement input
      RST      => not RESET_N,          -- 1-bit input: Active high reset input
      --!}
      --!\name Output signals
      --!{
      CLK0     => Clk50MHzDcm,          -- 1-bit output: 0 degree clock output
      CLK180   => open,  -- 1-bit output: 180 degree clock output
      CLK270   => open,  -- 1-bit output: 270 degree clock output
      CLK2X    => Clk100MHzDcm,  -- 1-bit output: 2X clock frequency clock output
      CLK2X180 => open,  -- 1-bit output: 2X clock frequency, 180 degree clock output
      CLK90    => open,                 -- 1-bit output: 90 degree clock output
      CLKDV    => open,                 -- 1-bit output: Divided clock output
      CLKFX    => open,  -- 1-bit output: Digital Frequency Synthesizer output (DFS)
      CLKFX180 => open,  -- 1-bit output: 180 degree CLKFX output
      LOCKED   => Reset_n_loc,          -- 1-bit output: DCM_SP Lock Output
      PSDONE   => open,  -- 1-bit output: Phase shift done output
      STATUS   => open                  -- 8-bit output: DCM_SP status output
     --!}
      )                                                                        ;

  -- *** BUFG
  -- BUFG: Global Clock Buffer
  -- Spartan-6
  -- Xilinx HDL Libraries Guide, version 14.7
  uBUFG_50MHz : BUFG
    port map (
      O => Clk50MHz,                    -- 1-bit output: Clock buffer output
      I => Clk50MHzDcm                  -- 1-bit input: Clock buffer input
      )                                                                        ;

  uBUFG_100MHz : BUFG
    port map (
      O => Clk100MHz,                   -- 1-bit output: Clock buffer output
      I => Clk100MHzDcm                 -- 1-bit input: Clock buffer input
      )                                                                        ;

end architecture RTL                                                           ;
-- * End of File
