-------------------------------------------------------------------------------
-- * Header
-------------------------------------------------------------------------------
--!  \project       Mojo V3
--!  \file          UserTop.vhd
--!  \author        Alvaro Cortes Sanchez-Migallon
--!  \email         alvarocsm.91@gmail.com
--!  \version       A/0
--!  \date          25/04/2020
--|----------------------------------------------------------------------------
--|  Description:
--!
--!  \class         VHDL soure file
--!
--!  \brief         This represent the top hierachy of the user functionality
--!                 design
--!
--!  \section links links
--!
--!  \section extra More information in Firmware specification document and
--!                 board documentation.
--!
--!  \section buses buses
--!
--'---------------------------------------------------------------------------'

-------------------------------------------------------------------------------
-- * Libraries used:
-------------------------------------------------------------------------------
-- ** Standar VHDL Packages
library IEEE;
use IEEE.std_logic_1164.all;
--use IEEE.std_logic_textio.all;
--use IEEE.std_logic_arith.all;
--use IEEE.numeric_bit.all;
use IEEE.numeric_std.all;
--use IEEE.std_logic_signed.all;
--use IEEE.std_logic_unsigned.all;
--use IEEE.math_real.all;
--use IEEE.math_complex.all;

--library STD;
--use STD.textio;

-- ** Primitive components
--library unisim;
--use unisim.vcomponents.all;

-------------------------------------------------------------------------------
-- * Packages used
-------------------------------------------------------------------------------
use work.design_pkg.all;

-------------------------------------------------------------------------------
-- * Entity definition
-------------------------------------------------------------------------------
entity UserTop is
  port (
    --!\name Bank 0
    --!{
    Clk50MHz : in  std_logic;
    --Clk1000MHz : in  std_logic;
    Reset_n  : in  std_logic;
    Led      : out std_logic_vector(C_NUM_LED-1 downto 0)
   --!}
    );
end entity UserTop;

-------------------------------------------------------------------------------
-- * Architecture body:
-------------------------------------------------------------------------------
architecture RTL of UserTop is

  -- ** Constant definition
  -- none here, (see ./../../IP/Pkg/design_pkg.vhd)

  -- ** Component Definition

  -- ** Signal definition
  --!\name Port signals
  --!{
  signal led_cnt : unsigned(C_NUM_LED-1 downto 0);
  --!}
  --!\name Counter process
  --!{
  signal cnt : unsigned(C_DWORD_B-1 downto 0) := (others => '0');
                                        --!}

-------------------------------------------------------------------------------
-- * Arch Begin
-------------------------------------------------------------------------------
begin

  -- ** Signal association

  -- *** Internal to outputs
  Led <= std_logic_vector(led_cnt);

  -- *** Inputs to internal


  -- ** Component Instantation

  -- ** Process

  -- *** Led test
  -- purpose: Process to test board leds with one second counter
  -- type   : sequential
  -- inputs : Clk50MHz, cnt
  -- outputs: led
  uLedTest: process (Clk50MHz) is
  begin  -- process uLedTest
    if rising_edge(Clk50MHz) then
      if Reset_n = '0' then
        led_cnt <= (others => '0');
        cnt <= (others => '0');
      else
        cnt <= cnt + 1;
        if (cnt = C_SEC_BIN) then
          led_cnt <= led_cnt + 1;
        end if;
      end if;
    end if;
  end process uLedTest;

end architecture RTL;
-- * End of File
