-------------------------------------------------------------------------------
-- * Header
-------------------------------------------------------------------------------
--!  \project       Mojo V3
--!  \file          UserTopTb.vhd
--!  \author        Alvaro Cortes Sanchez-Migallon
--!  \email         alvarocsm.91@gmail.com
--!  \version       A/0
--!  \date          16/05/2020
--|----------------------------------------------------------------------------
--|  Description:
--!
--!  \class         VHDL simulation file
--!
--!  \brief         This represent the top hierachy of the user functionality
--!                 design
--!
--!  \section links links
--!
--!  \section extra More information in Firmware specification document and
--!                 board documentation.
--!
--!  \section buses buses
--!
--'---------------------------------------------------------------------------'

-------------------------------------------------------------------------------
-- * Libraries used:
-------------------------------------------------------------------------------
-- ** Standar VHDL Packages
library IEEE;
use IEEE.std_logic_1164.all;
--use IEEE.std_logic_textio.all                                                ;
--use IEEE.std_logic_arith.all                                                 ;
--use IEEE.numeric_bit.all                                                     ;
use IEEE.numeric_std.all;
--use IEEE.std_logic_signed.all                                                ;
--use IEEE.std_logic_unsigned.all                                              ;
--use IEEE.math_real.all                                                       ;
--use IEEE.math_complex.all                                                    ;

--library STD                                                                  ;
--use STD.textio                                                               ;

-------------------------------------------------------------------------------
-- * Packages used
-------------------------------------------------------------------------------
use work.design_pkg.all;

-------------------------------------------------------------------------------
-- * Empty entity
-------------------------------------------------------------------------------
entity UserTopTb is

end entity UserTopTb;

-------------------------------------------------------------------------------
-- * Architecture body:
-------------------------------------------------------------------------------
architecture RTL of UserTopTb is


  -- ** Constant definition
  -- none here, (see ./../Pkg/design_pkg.vhd)

  -- *** Clock periods
  constant C_CLK_PERIOD : time := 20 ns;

  -- ** Signal definition
  --!\name DUT Ports to signals
  --!{
  -- DiscreteTest ports -------------------------------------------------------
  signal Clk     : std_logic;
  signal Reset_n : std_logic;
  signal Led     : std_logic_vector(C_NUM_LED-1 downto 0);
  -----------------------------------------------------------------------------
  --!}

  -- ** Component declaration

-------------------------------------------------------------------------------
-- * Arch Begin
-------------------------------------------------------------------------------
begin

  -- ** Signal association
  -- *** Internal to outputs

  -- *** Inputs to internal

  -- *** Internal interconnection

  -- ** Process

  -- *** Clocks
  uClk : process
  begin
    Clk <= '1';
    wait for C_CLK_PERIOD/2;
    Clk <= '0';
    wait for C_CLK_PERIOD/2;
  end process uClk;

  -- *** Reset_n
  uReset : process
  begin
    Reset_n <= '0';
    wait for (20*C_CLK_PERIOD+C_CLK_PERIOD/2);
    Reset_n <= '1';
    wait;
  end process uReset;

  -- *** Stimulus
  uStimulus : process
  begin

    -- **** Wait after reset
    wait for 20*C_CLK_PERIOD;

    -- **** Init Value signals

    -- **** Wait after reset
    wait for 100*C_CLK_PERIOD;

    -- **** Enable Module

    -- **** Wait forever
    wait;
  end process uStimulus;

  -- *** Test All input signals

  -- ** Component Instantation
  uUserTop : entity work.UserTop
    port map (
      Clk50MHz => Clk,
      Reset_n  => Reset_n,
      Led      => Led);

end architecture RTL;
