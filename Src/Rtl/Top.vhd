-------------------------------------------------------------------------------
-- * Header
-------------------------------------------------------------------------------
--!  \project        Mojo V3
--!  \file           Top.vhd
--!  \author         Alvaro Cortes Sanchez-Migallon
--!  \email          alvarocsm.91@gmail.com
--!  \version        A/0
--!  \date           13/05/2020
--|----------------------------------------------------------------------------
--|  Description:
--!
--!  \class         VHDL source file
--!
--!  \brief         This file represent the FPGA interface, require pinout.ucf
--!                 file to define constraint and use os FPGA pinour, is based
--!                 on SPARTAN 6 FGPA
--!
--!  \section links [[file:~/Documents/Projects/Github/MojoV3/Src/Constraints/Pinout.xdc][Pinout.xdc]]
--!
--!  \section extra More information in Firmware specification document and
--!                 board documentation.
--!
--!  \section buses buses
--!
--'---------------------------------------------------------------------------'

-------------------------------------------------------------------------------
-- * Libraries used:
-------------------------------------------------------------------------------
-- ** Standar VHDL Packages
library IEEE;
use IEEE.std_logic_1164.all;
--use IEEE.std_logic_textio.all;
--use IEEE.std_logic_arith.all;
--use IEEE.numeric_bit.all;
use IEEE.numeric_std.all;
--use IEEE.std_logic_signed.all;
--use IEEE.std_logic_unsigned.all;
--use IEEE.math_real.all;
--use IEEE.math_complex.all;

--library STD;
--use STD.textio;

-- ** Primitive components
--library unisim;
--use unisim.vcomponents.all;

-------------------------------------------------------------------------------
-- * Packages used
-------------------------------------------------------------------------------
use work.design_pkg.all;

-------------------------------------------------------------------------------
-- * Entity definition
-------------------------------------------------------------------------------
entity Top is
  port (
    --!\name Bank 0
    --!{
    IO_L1P_HSWAPEN_0           : in  std_logic;  -- LOC = "P144"
    IO_L1N_VREF_0              : in  std_logic;  -- LOC = "P143"
    IO_L2P_0                   : in  std_logic;  -- LOC = "P142"
    IO_L2N_0                   : in  std_logic;  -- LOC = "P141"
    IO_L3P_0                   : in  std_logic;  -- LOC = "P140"
    IO_L3N_0                   : in  std_logic;  -- LOC = "P139"
    IO_L4P_0                   : in  std_logic;  -- LOC = "P138"
    IO_L4N_0                   : in  std_logic;  -- LOC = "P137"
    IO_L34P_GCLK19_0           : out std_logic;  -- LOC = "P134"
    IO_L34N_GCLK18_0           : out std_logic;  -- LOC = "P133"
    IO_L35P_GCLK17_0           : out std_logic;  -- LOC = "P132"
    IO_L35N_GCLK16_0           : out std_logic;  -- LOC = "P131"
    IO_L36P_GCLK15_0           : out std_logic;  -- LOC = "P127"
    IO_L36N_GCLK14_0           : out std_logic;  -- LOC = "P126"
    IO_L37P_GCLK13_0           : out std_logic;  -- LOC = "P124"
    IO_L37N_GCLK12_0           : out std_logic;  -- LOC = "P123"
    IO_L62P_0                  : in  std_logic;  -- LOC = "P121"
    IO_L62N_VREF_0             : in  std_logic;  -- LOC = "P120"
    IO_L63P_SCP7_0             : in  std_logic;  -- LOC = "P119"
    IO_L63N_SCP6_0             : in  std_logic;  -- LOC = "P118"
    IO_L64P_SCP5_0             : in  std_logic;  -- LOC = "P117"
    IO_L64N_SCP4_0             : in  std_logic;  -- LOC = "P116"
    IO_L65P_SCP3_0             : in  std_logic;  -- LOC = "P115"
    IO_L65N_SCP2_0             : in  std_logic;  -- LOC = "P114"
    IO_L66P_SCP1_0             : in  std_logic;  -- LOC = "P112"
    IO_L66N_SCP0_0             : in  std_logic;  -- LOC = "P1
    --!}
    --!\name Bank 1
    --!{
    IO_L1P_1                   : in  std_logic;  -- LOC = "P105"
    IO_L1N_VREF_1              : in  std_logic;  -- LOC = "P104"
    IO_L32P_1                  : in  std_logic;  -- LOC = "P102"
    IO_L32N_1                  : in  std_logic;  -- LOC = "P101"
    IO_L33P_1                  : in  std_logic;  -- LOC = "P100"
    IO_L33N_1                  : in  std_logic;  -- LOC = "P99"
    IO_L34P_1                  : in  std_logic;  -- LOC = "P98"
    IO_L34N_1                  : in  std_logic;  -- LOC = "P97"
    IO_L40P_GCLK11_1           : in  std_logic;  -- LOC = "P95"
    IO_L40N_GCLK10_1           : in  std_logic;  -- LOC = "P94"
    IO_L41P_GCLK9_IRDY1_1      : in  std_logic;  -- LOC = "P93"
    IO_L41N_GCLK8_1            : in  std_logic;  -- LOC = "P92"
    IO_L42P_GCLK7_1            : in  std_logic;  -- LOC = "P88"
    IO_L42N_GCLK6_TRDY1_1      : in  std_logic;  -- LOC = "P87"
    IO_L43P_GCLK5_1            : in  std_logic;  -- LOC = "P85"
    IO_L43N_GCLK4_1            : in  std_logic;  -- LOC = "P84"
    IO_L45P_1                  : in  std_logic;  -- LOC = "P83"
    IO_L45N_1                  : in  std_logic;  -- LOC = "P82"
    IO_L46P_1                  : in  std_logic;  -- LOC = "P81"
    IO_L46N_1                  : in  std_logic;  -- LOC = "P80"
    IO_L47P_1                  : in  std_logic;  -- LOC = "P79"
    IO_L47N_1                  : in  std_logic;  -- LOC = "P78"
    IO_L74P_AWAKE_1            : in  std_logic;  -- LOC = "P75"
    IO_L74N_DOUT_BUSY_1        : in  std_logic;  -- LOC = "P74"
    --!}
    --!\name Bank 2
    --!{
    CMPCS_B_2                  : in  std_logic;  -- LOC = "P72"
    DONE_2                     : in  std_logic;  -- LOC = "P71"
    PROGRAM_B_2                : in  std_logic;  -- LOC = "P37"
    IO_L1P_CCLK_2              : in  std_logic;  -- LOC = "P70"
    IO_L1N_M0_CMPMISO_2        : in  std_logic;  -- LOC = "P69"
    IO_L2P_CMPCLK_2            : in  std_logic;  -- LOC = "P67"
    IO_L2N_CMPMOSI_2           : in  std_logic;  -- LOC = "P66"
    IO_L3P_D0_DIN_MISO_MISO1_2 : in  std_logic;  -- LOC = "P65"
    IO_L3N_MOSI_CSI_B_MISO0_2  : in  std_logic;  -- LOC = "P64"
    IO_L12P_D1_MISO2_2         : in  std_logic;  -- LOC = "P62"
    IO_L12N_D2_MISO3_2         : in  std_logic;  -- LOC = "P61"
    IO_L13P_M1_2               : in  std_logic;  -- LOC = "P60"
    IO_L13N_D10_2              : in  std_logic;  -- LOC = "P59"
    IO_L14P_D11_2              : in  std_logic;  -- LOC = "P58"
    IO_L14N_D12_2              : in  std_logic;  -- LOC = "P57"
    IO_L30P_GCLK1_D13_2        : in  std_logic;  -- LOC = "P56"
    IO_L30N_GCLK0_USERCCLK_2   : in  std_logic;  -- LOC = "P55"
    IO_L31P_GCLK31_D14_2       : in  std_logic;  -- LOC = "P51"
    IO_L31N_GCLK30_D15_2       : in  std_logic;  -- LOC = "P50"
    IO_L48P_D7_2               : in  std_logic;  -- LOC = "P48"
    IO_L48N_RDWR_B_VREF_2      : in  std_logic;  -- LOC = "P47"
    IO_L49P_D3_2               : in  std_logic;  -- LOC = "P46"
    IO_L49N_D4_2               : in  std_logic;  -- LOC = "P45"
    IO_L62P_D5_2               : in  std_logic;  -- LOC = "P44"
    IO_L62N_D6_2               : in  std_logic;  -- LOC = "P43"
    IO_L64P_D8_2               : in  std_logic;  -- LOC = "P41"
    IO_L64N_D9_2               : in  std_logic;  -- LOC = "P40"
    IO_L65P_INIT_B_2           : in  std_logic;  -- LOC = "P39"
    IO_L65N_CSO_B_2            : in  std_logic;  -- LOC = "P38"
    --!}
    --!\name Bank 3
    --!{
    IO_L1P_3                   : in  std_logic;  -- LOC = "P35"
    IO_L1N_VREF_3              : in  std_logic;  -- LOC = "P34"
    IO_L2P_3                   : in  std_logic;  -- LOC = "P33"
    IO_L2N_3                   : in  std_logic;  -- LOC = "P32"
    IO_L36P_3                  : in  std_logic;  -- LOC = "P30"
    IO_L36N_3                  : in  std_logic;  -- LOC = "P29"
    IO_L37P_3                  : in  std_logic;  -- LOC = "P27"
    IO_L37N_3                  : in  std_logic;  -- LOC = "P26"
    IO_L41P_GCLK27_3           : in  std_logic;  -- LOC = "P24"
    IO_L41N_GCLK26_3           : in  std_logic;  -- LOC = "P23"
    IO_L42P_GCLK25_TRDY2_3     : in  std_logic;  -- LOC = "P22"
    IO_L42N_GCLK24_3           : in  std_logic;  -- LOC = "P21"
    IO_L43P_GCLK23_3           : in  std_logic;  -- LOC = "P17"
    IO_L43N_GCLK22_IRDY2_3     : in  std_logic;  -- LOC = "P16"
    IO_L44P_GCLK21_3           : in  std_logic;  -- LOC = "P15"
    IO_L44N_GCLK20_3           : in  std_logic;  -- LOC = "P14"
    IO_L49P_3                  : in  std_logic;  -- LOC = "P12"
    IO_L49N_3                  : in  std_logic;  -- LOC = "P11"
    IO_L50P_3                  : in  std_logic;  -- LOC = "P10"
    IO_L50N_3                  : in  std_logic;  -- LOC = "P9"
    IO_L51P_3                  : in  std_logic;  -- LOC = "P8"
    IO_L51N_3                  : in  std_logic;  -- LOC = "P7"
    IO_L52P_3                  : in  std_logic;  -- LOC = "P6"
    IO_L52N_3                  : in  std_logic;  -- LOC = "P5"
    IO_L83P_3                  : in  std_logic;  -- LOC = "P2"
    IO_L83N_VREF_3             : in  std_logic   -- LOC = "P1"
   --!}
    );
end entity Top;

-------------------------------------------------------------------------------
-- * Architecture body:
-------------------------------------------------------------------------------
architecture RTL of Top is

  -- ** Constant definition
  -- none here, (see ./../../IP/Pkg/design_pkg.vhd)

  -- ** Signal definition
  signal clk     : std_logic;                               -- External clock
  signal reset_n : std_logic;                               -- External reset
  signal led     : std_logic_vector(C_NUM_LED-1 downto 0);  -- External leds

-------------------------------------------------------------------------------
-- * Arch Begin
-------------------------------------------------------------------------------
begin

  -- ** Signal association

  -- *** Internal to outputs
  -- **** Led, output
  IO_L34P_GCLK19_0 <= led(0);
  IO_L34N_GCLK18_0 <= led(1);
  IO_L35P_GCLK17_0 <= led(2);
  IO_L35N_GCLK16_0 <= led(3);
  IO_L36P_GCLK15_0 <= led(4);
  IO_L36N_GCLK14_0 <= led(5);
  IO_L37P_GCLK13_0 <= led(6);
  IO_L37N_GCLK12_0 <= led(7);

  -- *** Inputs to internal
  -- **** External clock
  clk     <= IO_L30P_GCLK1_D13_2;
  reset_n <= IO_L65N_CSO_B_2;

  -- ** Component Instantation
  -- *** HAL
  uHAL : entity work.HAL
    port map (
      CLK     => clk,
      RESET_N => reset_n,
      LED     => led);

end architecture RTL;
-- * End of File
