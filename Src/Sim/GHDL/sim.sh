###############################################################################
# * Header
###############################################################################
#!  \project        Mojo V3
#!  \file           sim.sh
#!  \author         Alvaro Cortes Sanchez-Migallon
#!  \email          alvarocsm.91@gmail.com
#!  \version        A/0
#!  \date           13/05/2020
#|#############################################################################
#|  Description:
#!
#!  \class         GHDL script source
#!
#!  \brief         This file contains commands to analyze, elaborate and
#!                 visualizate differents test bench with GHDL
#!
#!  \section links [[https://ghdl.readthedocs.io/en/latest/index.html][ghdl documentation]]
#!
#!  \section extra More information in Firmware specification document and
#!                 board documentation.
#!
#'#############################################################################

###############################################################################
# * Function define
###############################################################################
# ** Help
help()
{
    echo ""
    echo "Execute ghdl with different arguments to use different methods:"
    echo "./ghdl 0: Clean simulation environment"
    echo "./ghdl 1: Setup simulation environment"
    echo "./ghdl 2: Include all files in worlLib"
    echo "./ghdl 3: Analyze all files with make"
    echo "./ghdl 4: Elaborate UserTop 50ms"
    echo "./ghdl 5: Generate waveforms to gtkwave representation"
    echo "./ghdl 6: Generate Makefile"
    echo "./ghdl 7: All process"
    echo ""
}

# ** 0. Clean directory
clean (){
    rm -rf e* u* w* M*
    # *** Clean up directories
    rm usertoptb *.o workLib/* *~
}

# ** 1. Make setup
makeSetUp(){
    # ** Check if directories exist
    # *** workLib to objects
    if [ -d workLib ]; then
        echo "Directorie workLib to store object files"
    else
        echo "Create workLib directorie to store object files"
        mkdir workLib
    fi

    # *** ghdlWork to executable files
    if [ -d executablesTb ]; then
        echo "Directory executablesTb to store executable files"
    else
        echo "Create executableTb directory to store executable files"
        mkdir executablesTb
    fi

    # *** Waveform files
    if [ -d waveforms ]; then
        echo "Directorie waveforms to store executable files"
    else
        echo "Create waveforms directory to store waveforms files"
        mkdir waveforms
    fi
}

# ** 2. Include ghdl files
includeGHDL(){
    # ** Include all sythetized files in workLib
    echo ".-------------------------------."
    echo "|    Including all vhdl files   |"
    echo ".-------------------------------."

    # *** File to list vhdl files
    find ../../. -name "*.vhd" > vhdFiles.txt

    # *** Include vhd files in FullWorkLib library
    for line in $(cat vhdFiles.txt);
    do
        echo "$line";
        #ghdl -i --std=93 --work=FullWorkLib --workdir=workLib --mb-comments $line
        ghdl -i --std=08 --work=FullWorkLib --workdir=workLib --mb-comments $line
    done
    echo ".-------------------------------."
    echo "|  Including all vhdl libraries |"
    echo ".-------------------------------."

#    # *** File to list vhdl files
    #    find /opt/Xilinx/14.7/ISE_DS/ISE/vhdl/src/unisims/primitive/ -name "*.vhd" > vhdLibs.txt
    #
    #    # *** Include vhd files in FullWorkLib library
    #    for line in $(cat vhdLibs.txt);
    #    do
    #        echo "$line";
    #        ghdl -i --std=08 --work=FullWorkLib --workdir=workLib --mb-comments $line
    #    done

    # *** Remove temp vhdl files
    rm vhdFiles.txt
    #rm vhdLibs.txt
}

# ** 3. Analyze and generate executables ghdl files
analyze(){
    #ghdl -m --std=08 --work=FullWorkLib --workdir=workLib --ieee=synopsys --mb-comments Top
    ghdl -m --std=08 --work=FullWorkLib --workdir=workLib --mb-comments UserTopTb
    #ghdl -a --std=08 --work=FullWorkLib --workdir=workLib --mb-comments UserTopTb
}

# ** 4. Elaborate and generate executables ghdl files
elaborate () {
    # Using mcode backend
    #ghdl -e --std=08 --work=FullWorkLib --workdir=workLib --mb-comments UserTopTb
    # Using gcc or llvm backend
    #time ./usertoptb --stop-time=5sec
    time ./usertoptb --stop-time=50ms
}

# ** 5. Simulate executables ghdl files
simFunct () {
    # mcode
    #ghdl -r --std=08 --work=FullWorkLib --workdir=workLib --mb-comments usertoptb --wave=waveforms/usertoptb.ghw --assert-level=note --stop-time=5sec
    # time command to get time execution
    time ghdl -r usertoptb --wave=usertoptb.ghw  --stop-time=50ms
    #ghdl -r usertoptb --wave=waveforms/usertoptb.ghw
    #mv usertoptb executablesTb/
    #gtkwave waveforms/usertoptb.ghw
}

# ** 6. Generate Makefile
genMakefile(){
    # acsm
    #ghdl --gen-makefile --std=08 --work=FullWorkLib --workdir=workLib --mb-comments usertoptb >> Makefile

    # csb
    #ghdl --gen-makefile -g -O2 --work=ghdl_work --workdir=ghdl_work --std=08 --ieee=synopsys -P/home/csbuendia/CompiledLibraries/Vivado --mb-comments --warn-unused -fexplicit -frelaxed-rules --no-vital-checks -fpsl averager_tb

    #ghdl --gen-makefile -g --std=93 --work=FullWorkLib --workdir=workLib --mb-comments --warn-unused -fexplicit -frelaxed-rules usertoptb >> Makefile
    ghdl --gen-makefile -g --std=08 --work=FullWorkLib --workdir=workLib --mb-comments --warn-unused -fexplicit -frelaxed-rules usertoptb >> Makefile
}

# ** Generate all Test Bench executables

# *** dds_tb
#ExecutableFile = "DDS_tb"
#echo $ExecutableFile
#ghdl -m --std=08 --work=FullWorkLib --workdir=workLib --mb-comments $ExecutableFile
#ExecutableFile = $ExecutableFile | tr '[:upper:]' '[:lower:]'
#echo $ExecutableFile
#mv $ExecutableFile executableFiles

# *** Build list with all executables files to move to executablesTb folder
#find . -executable -type -f > excutableFiles.txt
#for line in $(cat executableFiles.txt );
#do
#    echo "$line";
#    if [ "$line" == sim.sh ]
#       echo "Sim existe"
#then
#    mv $line executablesTb/
#fi
#done

# *** Move executable to executable files

###############################################################################
# * Main while script
###############################################################################
Input=$1

# ** while loop
while :
do
    case $Input in
        0) clean
           break ;;

        1) makeSetUp
           break ;;

        2) includeGHDL
           break ;;

        3) analyze
           break ;;

        4) elaborate
           break ;;

        5) simFunct
           break ;;

        6) genMakefile
           break ;;

        7) clean
           makeSetUp
           includeGHDL
           analyze
           elaborate
           simFunct
           break ;;

        --help) help
                break ;;
        *) echo "Introduce: --help"
           read Input
           ;;

    esac
done
